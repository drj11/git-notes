# Useful git commands and commands I can never quite remember

## The best command ever

    git log --decorate --oneline --graph --all

Run this all the time.


## "Universal" push command

    git push origin HEAD
    
I strongly recommend the 4 word form of push (as above).
Using `HEAD` is magic because it will push the current HEAD
to the corresponding ref on the remote.

Note that `origin` is not magic,
it is the conventional name for "the usual remote";
another remote can be named (or you can give the URL explicitly).


## fetch instead of pull

    git fetch --prune --all


## get a checksum SHA

    git rev-parse HEAD


## Where am I?

Find this commit relative to nearby tags:

    git describe --always --long --dirty

If you want to know about nearby branches too, add `--all`:

    git describe --all --always --long --dirty


## Common ancestor

    git merge-base A B

But if you want to take a diff from the common ancestory,
you can go direct:
`git diff A...B` uses common ancestor as the _from_ version.


## Commits between A and B

    git rev-list A..B

Shows commits "in B" that are "not in A".
Technically: reachable from B, but not reachable from A.

Also possibly of use:

    git describe --all commit

    git tag --contains


## grep the log

    git log --grep 'thing'

Only searches from current branch.
Commits that are only on an unmerged branch won't be discovered.
Unless you add the `--all` option:

    git log --all --grep 'thing'


## Has this commit been merged?

Into what? If you have a commit X in hand,

    git branch --contains X

will list which branches have X as an ancestor.
Which is probably what you want when you say
"branch Y contains commit X, or commit X is merged into branch Y".

X can be a commit SHA or a branch/tag and defaults to HEAD.

So for an ordinary branch BR,

    git branch --contains BR

will list the branches into which BR has been merged.

Usually we are interested in whether `master` (or `main`) is
listed in these branches.


## What branches need merging?

I can't speak to the requirements of your workflow.
Whether one branch _needs_ merging into another is a matter of
human process.

But if you want to see which branches have not been merged into
the branch called _master_, then that's:

    git branch -a --no-merge master

You can replace _master_ with any other commit/branch
to see what branches have no been merged into it.

The `-a` option is optional and means that
remote tracking branch are also included in the output.


## Push all branches

USE WITH CAUTION:

    git push origin --all
